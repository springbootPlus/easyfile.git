package com.github.springbootPlus.config.utils;

import org.junit.Test;

import java.io.IOException;
import java.util.Properties;

/**
 * @className: PropertiesUtilsTest
 * @description:
 * @author: WANGHUI
 * @createDate: 2018/6/6 17:54
 * @version: 1.0
 */
public class PropertiesUtilsTest {

    @Test
    public void load() throws IOException {
        Properties properties = PropertiesUtils.load("application.properties");
        System.out.println(properties);
    }

    @Test
    public void replacePlaceholders() {
        Properties properties = new Properties();
        properties.put("id", "aaa");
        PropertyPlaceholderHelper helper = new PropertyPlaceholderHelper("${", "}");
        String val = helper.replacePlaceholders("${TEMP}>>>123", properties);
        System.out.println(val);
    }
}