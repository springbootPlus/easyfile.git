package com.github.springbootPlus.config.utils;

import org.junit.Test;

import java.util.Set;

/**
 * @className: PackageHelperTest
 * @description:
 * @author: WANGHUI
 * @createDate: 2018/6/6 18:20
 * @version: 1.0
 */
public class PackageHelperTest {

    @Test
    public void convertTypeAliasesPackage() {
        String[] classSet = PackageHelper.convertTypeAliasesPackage("com.github.**");
        for (String aClass : classSet) {
            System.out.println(aClass);
        }
    }

    @Test
    public void scanTypePackage() {

        Set<Class> classSet = PackageHelper.scanTypePackage("com.github.**");
        for (Class aClass : classSet) {
            System.out.println(aClass);
        }
    }

    @Test
    public void scanTypePackage1() {
        Set<Class> classSet = PackageHelper.scanTypePackage("org.github.**");
        for (Class aClass : classSet) {
            System.out.println(aClass);
        }
    }
}