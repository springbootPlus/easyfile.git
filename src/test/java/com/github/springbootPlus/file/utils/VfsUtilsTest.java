package com.github.springbootPlus.file.utils;

import com.github.springbootPlus.excel.parsing.AbstractExcelResolver;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @className: VfsUtilsTest
 * @description:
 * @author: WANGHUI
 * @createDate: 2018/6/6 16:21
 * @version: 1.0
 */
public class VfsUtilsTest {


    @Test
    public void readFile() throws IOException {
        InputStream in = VfsUtils.getInputStream("D:\\m2\\repository-spring5\\com\\github\\springbootPlus\\easyfile\\1.0.0\\_remote.repositories");
        String ii = AbstractExcelResolver.FileUtils.copyToString(in);
        System.out.println(ii);
    }


}