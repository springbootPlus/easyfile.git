package com.github.springbootPlus.file;

import com.github.springbootPlus.config.utils.ResourceUtils;
import com.github.springbootPlus.excel.parsing.AbstractExcelResolver;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @className: VfsFileSystemTest
 * @description:
 * @author: WANGHUI
 * @createDate: 2018/6/6 16:56
 * @version: 1.0
 */
public class VfsFileSystemTest {

    String dir = "sftp://root:root123456@xxxxx:22/data/";

    FileSystem fileSystem = new VfsFileSystem();

    @Test
    public void upload() throws IOException {
        InputStream in = ResourceUtils.getResourceFromClassPath("log4j2.xml").getInputStream();
        fileSystem.upload(in, dir + "log4j2.xml");
    }

    @Test
    public void download() throws IOException {
        String fileUri = dir + "log4j2.xml";
        InputStream in = fileSystem.download(fileUri);
        System.out.println(AbstractExcelResolver.FileUtils.copyToString(in));
    }
}