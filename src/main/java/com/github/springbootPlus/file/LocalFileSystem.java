package com.github.springbootPlus.file;


import org.springframework.util.FileCopyUtils;

import java.io.*;


public class LocalFileSystem implements FileSystem {

    @Override
    public void upload(InputStream inputStream, String fileUri) throws IOException {
        File file = new File(fileUri);
        FileCopyUtils.copy(inputStream, new FileOutputStream(file));
    }

    @Override
    public InputStream download(String fileUri) throws IOException {
        File file = new File(fileUri);
        return new FileInputStream(file);
    }

}
