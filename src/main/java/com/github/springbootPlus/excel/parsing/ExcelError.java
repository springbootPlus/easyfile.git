package com.github.springbootPlus.excel.parsing;

/**
 * Excel 导入时产生的错误消息
 *
 * @author lisuo
 */
public class ExcelError {

    /**
     * 第几行
     */
    private int row;
    /**
     * 错误消息
     */
    private String errorMsg;

    public ExcelError(int row, String errorMsg) {
        this.row = row;
        this.errorMsg = errorMsg;
    }

    public int getRow() {
        return row;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExcelError error = (ExcelError) o;

        if (row != error.row) return false;
        return errorMsg != null ? errorMsg.equals(error.errorMsg) : error.errorMsg == null;
    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + (errorMsg != null ? errorMsg.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ExcelError{" +
                "row=" + row +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
