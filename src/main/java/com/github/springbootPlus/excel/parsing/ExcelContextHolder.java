package com.github.springbootPlus.excel.parsing;

import java.util.Map;

public class ExcelContextHolder {

    private static ThreadLocal<Map<String, Object>> context = new ThreadLocal<Map<String, Object>>();

    public static void setContext(Map<String, Object> param) {
        context.set(param);
    }

    public static Map<String, Object> getContext() {
        return context.get();
    }

    public static Object getValue(String key) {
        return context.get() == null ? null : context.get().get(key);
    }

    public static void removeContext() {
        context.remove();
    }

}
