package com.github.springbootPlus.excel.parsing;

import java.util.List;

/**
 * Created by Administrator on 2017/8/4.
 */
public interface RowFilter<T> {
    /**
     * 操作类型，导入或导出
     */
    enum Type {
        EXPORT, IMPORT
    }

    public boolean isValid(T bean, Type type, int rowNum, List<ExcelError> errors) throws Exception;
}
