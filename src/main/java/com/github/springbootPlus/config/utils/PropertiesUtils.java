package com.github.springbootPlus.config.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.DefaultPropertiesPersister;
import org.springframework.util.PropertiesPersister;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.UnknownHostException;
import java.util.Properties;

/**
 * @className: PropertiesUtils
 * @description:
 * @author: WANGHUI
 * @createDate: 2018/5/11 9:55
 * @version: 1.0
 */
public abstract class PropertiesUtils extends PropertiesLoaderUtils {

    protected static final Log logger = LogFactory.getLog(PropertiesUtils.class);

    private static final String XML_FILE_EXTENSION = ".xml";

    private static final String fileEncoding = "UTF-8";

    private static final boolean ignoreResourceNotFound = true;

    public static Properties load(String propertiesFile) throws IOException {
        Resource in = new DefaultResourceLoader().getResource(propertiesFile);
        return loadProperties(in);
    }

    public static Properties load(String[] locations) throws IOException {
        Properties props = new Properties();
        for (String location : locations) {
            if (logger.isDebugEnabled()) {
                logger.debug("Loading properties file from " + location);
            }
            try {
                Resource in = new DefaultResourceLoader().getResource(location);
                fillProperties(
                        props, new EncodedResource(in, fileEncoding), new DefaultPropertiesPersister());
            } catch (IOException ex) {
                // Resource not found when trying to open it
                if (ignoreResourceNotFound &&
                        (ex instanceof FileNotFoundException || ex instanceof UnknownHostException)) {
                    if (logger.isInfoEnabled()) {
                        logger.info("Properties resource not found: " + ex.getMessage());
                    }
                } else {
                    throw ex;
                }
            }
        }
        return props;
    }

    public static void fillProperties(Properties props, EncodedResource resource, PropertiesPersister persister)
            throws IOException {

        InputStream stream = null;
        Reader reader = null;
        try {
            String filename = resource.getResource().getFilename();
            if (filename != null && filename.endsWith(XML_FILE_EXTENSION)) {
                stream = resource.getInputStream();
                persister.loadFromXml(props, stream);
            } else if (resource.requiresReader()) {
                reader = resource.getReader();
                persister.load(props, reader);
            } else {
                stream = resource.getInputStream();
                persister.load(props, stream);
            }
        } finally {
            if (stream != null) {
                stream.close();
            }
            if (reader != null) {
                reader.close();
            }
        }
    }

}
