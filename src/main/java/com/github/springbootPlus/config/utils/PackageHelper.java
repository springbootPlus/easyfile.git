/**
 * Copyright (c) 2018-20XX, WANGHUI (18971286027@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.springbootPlus.config.utils;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.util.ClassUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 包扫描辅助类,读取类的相关信息
 * </p>
 *
 * @author wanghui
 * @Date 2018-05-02
 */
public class PackageHelper {


    private static final String RESOURCE_PATTERN = "/**/*.class";

    /**
     * <p>
     * 别名通配符设置
     * </p>
     * <p>
     * <property name="typeAliasesPackage" value="com.baomidou.*.entity"/>
     * </p>
     *
     * @param typeAliasesPackage 类别名包路径
     * @return 类的所在包路径
     */
    public static String[] convertTypeAliasesPackage(String typeAliasesPackage) {
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resolver);
        String pkg = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX
                + ClassUtils.convertClassNameToResourcePath(typeAliasesPackage) + "/*.class";
        /*
         * 将加载多个绝对匹配的所有Resource
         * 将首先通过ClassLoader.getResourceFromJar("META-INF")加载非模式路径部分，然后进行遍历模式匹配，排除重复包路径
         */
        try {
            Set<String> set = new HashSet<>();
            Resource[] resources = resolver.getResources(pkg);
            if (resources != null && resources.length > 0) {
                MetadataReader metadataReader;
                for (Resource resource : resources) {
                    if (resource.isReadable()) {
                        metadataReader = metadataReaderFactory.getMetadataReader(resource);
                        set.add(Class.forName(metadataReader.getClassMetadata().getClassName()).getPackage().getName());
                    }
                }
            }
            if (!set.isEmpty()) {
                return set.toArray(new String[]{});
            } else {
                throw new RuntimeException("not find typeAliasesPackage:" + pkg);
            }
        } catch (Exception e) {
            throw new RuntimeException("not find typeAliasesPackage:" + pkg, e);
        }
    }


    /**
     * <p>
     * 扫描获取指定包路径所有类
     * </p>
     *
     * @param typePackage 扫描类包路径
     * @return
     */
    public static Set<Class> scanTypePackage(String typePackage) {
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resolver);
        String pkg = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX
                + ClassUtils.convertClassNameToResourcePath(typePackage) + "/*.class";
        /*
         * 将加载多个绝对匹配的所有Resource
         * 将首先通过ClassLoader.getResourceFromJar("META-INF")加载非模式路径部分，然后进行遍历模式匹配，排除重复包路径
         */
        try {
            Set<Class> set = new HashSet<>();
            Resource[] resources = resolver.getResources(pkg);
            if (resources != null && resources.length > 0) {
                MetadataReader metadataReader;
                for (Resource resource : resources) {
                    if (resource.isReadable()) {
                        metadataReader = metadataReaderFactory.getMetadataReader(resource);
                        set.add(Class.forName(metadataReader.getClassMetadata().getClassName()));
                    }
                }
            }
            if (set.isEmpty()) {
                throw new RuntimeException("not find scanTypePackage:" + pkg);
            } else {
                return set;
            }
        } catch (Exception e) {
            throw new RuntimeException("not find scanTypePackage:" + pkg, e);
        }
    }

    /**
     * 将符合条件的Bean以Class集合的形式返回
     *
     * @param packagesToScan
     * @param annotations
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Set<Class> scanTypePackage(String[] packagesToScan, Class<? extends Annotation>... annotations) throws IOException, ClassNotFoundException {
        Set<Class> classSet = new HashSet<>();
        // 1.
        List<TypeFilter> typeFilters = new LinkedList<TypeFilter>();
        if (annotations != null) {
            for (Class<? extends Annotation> annotation : annotations) {
                typeFilters.add(new AnnotationTypeFilter(annotation, true));
            }
        }
        // 2.
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        MetadataReaderFactory readerFactory = new CachingMetadataReaderFactory(resolver);
        for (String pkg : packagesToScan) {
            String pattern = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
                    ClassUtils.convertClassNameToResourcePath(pkg) + RESOURCE_PATTERN;
            Resource[] resources = resolver.getResources(pattern);
            for (Resource resource : resources) {
                if (resource.isReadable()) {
                    MetadataReader reader = readerFactory.getMetadataReader(resource);
                    if (matchesEntityTypeFilter(typeFilters, reader, readerFactory)) {
                        String className = reader.getClassMetadata().getClassName();
                        classSet.add(Class.forName(className));
                    }
                }
            }
        }
        return classSet;
    }


    /**
     * 检查当前扫描到的Bean含有任何一个指定的注解标记
     *
     * @param reader
     * @param readerFactory
     * @return
     * @throws IOException
     */
    private static boolean matchesEntityTypeFilter(List<TypeFilter> typeFilters, MetadataReader reader, MetadataReaderFactory readerFactory) throws IOException {
        if (typeFilters == null || typeFilters.isEmpty())
            return true;

        for (TypeFilter filter : typeFilters) {
            if (filter.match(reader, readerFactory)) {
                return true;
            }
        }

        return false;
    }
}
