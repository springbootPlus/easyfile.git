package com.github.springbootPlus.config.utils;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * @className: PropertyPlaceholderConfigurer
 * @description:
 * @author: WANGHUI
 * @createDate: 2018/6/7 9:35
 * @version: 1.0
 */
public class PropertyPlaceholderConfigurer extends org.springframework.beans.factory.config.PropertyPlaceholderConfigurer {

    /**
     * 使spring的配置信息暴露出来
     */
    Properties exposedProp = new Properties();

    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props) throws BeansException {
        super.processProperties(beanFactoryToProcess, props);
//        exposedProp = props ;
    }


    /**
     * COPY mergeProperties返回值，而不要直接使用=；避免由此导致的spring配置信息的修改，此处只读
     *
     * @return
     * @throws IOException
     */
    public Properties getExposedProp() throws IOException {
        exposedProp.putAll(super.mergeProperties());
        return exposedProp;
    }


}
