# easyfile

#### 项目介绍
简化文件相关的操作

1.上传下载

2.office，pdf等文档的操作封装

3. excel导入导出

#### 软件架构
软件架构说明


#### 安装教程

1. 引入依赖

```
<dependency>
  <groupId>com.github.springbootPlus</groupId>
  <artifactId>easyfile</artifactId>
  <version>1.0.0</version>
</dependency>
```

#### 使用说明

1. excel导入

java代码：
````
 ExcelContext context = new ExcelContext("job-config.xml");
 Map param = Collections.singletonMap("companyId", "cccccc");
 InputStream in = ResourceUtils.getResourceFromClassPath("job.xlsx").getInputStream();
 ExcelImportResult result = context.readExcel("jobCategory", in, param);
        
````
job-config.xml
````
<?xml version="1.0" encoding="UTF-8"?>
<excels>
    <!-- excel标签参看:ExcelDefinition,Field标签参看:FieldValue -->

    <excel id="jobCategory" class="Job" titleIndex="0" uniqueFields="jobCode"
   		uniqueDesc=" 职业编码重复 " rowFilter="com.kbao.sales.jobCategory.service.JobRowFilter">
        <field name="companyId"  title="companyId" defaultValue ="${companyId}"/>
        <field name="jobTypeCode" title="职业大类代码" isNull="false"/>
        <field name="jobTypeName" title="职业大类名称"  isNull="false" />
        <field name="jobCode" title="职业编码"  isNull="false"/>
        <field name="jobName" title="职业名称"  isNull="false"/>
        <field name="jobLevel" title="职业等级"  isNull="false"/>
        <field name="createTime" title="创建时间" defaultValue ="newDate()"/>
    </excel>
</excels>

````

job.xlsx的标题和xml中的title保持一致

|职业大类代码	|职业大类名称	|职业编码	|职业名称	|职业等级|
| - | :-: |:-: |:-: | -: | 
|101|机关团体公司行号|H0101003|	私营企业主（不亲自作业）|1|


导入的校验规则

|校验类型	|属性名	| XML节点位置	|描述|
| - | :-: |:-:   | :-| 
|是否可空	| isNull	| excel/field/isNull| 内容是否可空 |
|正则校验	| regex	| excel/field/regex | 通过正则判断内容是否合法  |
|默认值	| defaultValue	| excel/field/defaultValue | 通过正则判断内容是否合法  |
|主键字段是否重复	| uniqueFields	| excel/uniqueFields | 通过主键字段判断行数据内容是否重复  |
|判断导入内容是否已在DB中存在	| rowFilter	| excel/rowFilter | 判断导入行是否已在DB中存在，详见 RowFilter接口 |

2. excel导出

java代码：
````
ExcelContext context = new ExcelContext("excel/order-config.xml");
List<Pojo> beanList = new ArrayList<Pojo>(); //从数据库中查询数据
File file = context.createExcelFile("insureOrder", beanList);

````

3. 使用VfsFileSystem实现上传下载，支持的常规协议sftp,ftp,local,http;
[Supported File Systems](http://commons.apache.org/proper/commons-vfs/filesystems.html) 
JAVA代码：

````
String dir = "sftp://root:root123456@xxxxx:22/data/";

FileSystem fileSystem = new VfsFileSystem();

InputStream in = ResourceUtils.getResourceFromClassPath("log4j2.xml").getInputStream();
fileSystem.upload(in, dir+"log4j2.xml");

InputStream in = fileSystem.download(fileUri);
System.out.println(FileUtils.copyToString(in));

````

4. 加载classpath或者Jar包中的配置文件，使用配置信息替换占位符

加载配置文件
````
 Properties properties = PropertiesUtils.load("application.properties");
 
````

替换占位符
````
  PropertyPlaceholderHelper helper = new PropertyPlaceholderHelper("${", "}");
  String val = helper.replacePlaceholders("${TEMP}>>>123", properties);
  System.out.println(val);
  // result :  C:\Users\WANGHUI\AppData\Local\Temp>>>123
````

5.扫包工具，根据包路径读取class类的信息，实现读取注解等功能

````
    Set<Class> classSet =  PackageHelper.scanTypePackage("com.github.**");
    for (Class aClass : classSet) {
        System.out.println(aClass);
    }
````

#### 参与贡献

1. Fork 本项目
2. 新建 youname_xxx 分支
3. 提交代码
4. 新建 Pull Request

